## Installation guide
- Requires a working version of NodeJS and NPM
- Open the terminal in the folder you want to clone the project
- Clone the project
- If not installed yet, install typings
> $ npm install -g typings
- After npm is done installing typings, install the rest of the dependencies
> $ npm install
- After npm is done installing, run
> $ npm watch
- or open the index file on a running web server