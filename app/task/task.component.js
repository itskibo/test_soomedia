"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var Task = (function () {
    function Task() {
        this._id = null;
        this._finished = false;
        this._editable = false;
        this.onDelete = new core_1.EventEmitter();
        this.onFinished = new core_1.EventEmitter();
    }
    Object.defineProperty(Task.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (id) {
            this._id = id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Task.prototype, "title", {
        get: function () {
            return this._title;
        },
        set: function (title) {
            this._title = title;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Task.prototype, "finished", {
        get: function () {
            return this._finished;
        },
        set: function (finished) {
            this._finished = finished;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Task.prototype, "editable", {
        get: function () {
            return this._editable;
        },
        set: function (editable) {
            this._editable = editable;
        },
        enumerable: true,
        configurable: true
    });
    Task.prototype.toggleEditable = function () {
        this._editable = !this._editable;
    };
    Task.prototype.deleteTask = function (id) {
        this.onDelete.emit(id);
    };
    Task.prototype.finishTask = function (id) {
        this._finished = !this._finished;
        this.onFinished.emit(id);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], Task.prototype, "_title", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], Task.prototype, "_id", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], Task.prototype, "_finished", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], Task.prototype, "onDelete", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], Task.prototype, "onFinished", void 0);
    Task = __decorate([
        core_1.Component({
            selector: 'task',
            moduleId: module.id,
            inputs: ['id', 'title', 'finished'],
            templateUrl: 'task.template.html',
            styleUrls: ['task.style.css']
        }), 
        __metadata('design:paramtypes', [])
    ], Task);
    return Task;
}());
exports.Task = Task;
