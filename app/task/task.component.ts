import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
	selector: 'task',
	moduleId: module.id,
	inputs: ['id', 'title', 'finished'],
	templateUrl: 'task.template.html',
	styleUrls: ['task.style.css']
})
export class Task {
	@Input()
	_title: string;

	
	@Input()
	_id: number = null;

	@Input()
	_finished: boolean = false;

	_editable: boolean = false;

	@Output()
	onDelete = new EventEmitter<number>();

	@Output()
	onFinished = new EventEmitter<number>();

	get id() : number {
		return this._id;
	}

	set id(id: number) {
		this._id = id;
	}

	get title() : string {
		return this._title;
	}

	set title(title: string) {
		this._title = title;
	}

	get finished() : boolean {
		return this._finished;
	}

	set finished(finished: boolean) {
		this._finished = finished;
	}

	get editable() : boolean {
		return this._editable;
	}

	set editable(editable: boolean) {
		this._editable = editable;
	}

	toggleEditable() {
		this._editable = !this._editable;
	}

	deleteTask(id: number) {
		this.onDelete.emit(id);
	}

	finishTask(id: number) {
		this._finished = !this._finished;
		this.onFinished.emit(id);
	}
}