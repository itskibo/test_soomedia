import {enableProdMode} from '@angular/core';
import {bootstrap} from '@angular/platform-browser-dynamic';
import {disableDeprecatedForms, provideForms} from '@angular/forms';
import {BaseComponent} from './base/base.component';

enableProdMode();

bootstrap(BaseComponent, [
	disableDeprecatedForms(),
	provideForms()
])
.catch((err: any) => console.error(err));