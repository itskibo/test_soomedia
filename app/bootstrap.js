"use strict";
var core_1 = require('@angular/core');
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var forms_1 = require('@angular/forms');
var base_component_1 = require('./base/base.component');
core_1.enableProdMode();
platform_browser_dynamic_1.bootstrap(base_component_1.BaseComponent, [
    forms_1.disableDeprecatedForms(),
    forms_1.provideForms()
])
    .catch(function (err) { return console.error(err); });
