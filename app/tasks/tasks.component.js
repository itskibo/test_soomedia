"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var task_component_1 = require('./../task/task.component');
var TasksComponent = (function () {
    function TasksComponent() {
        this._tasks = [];
        this._filteredTasks = [];
        this._tasksLength = 0;
        this._bShowAddForm = true;
        this._bShowSortForm = false;
        this._bShowFilterForm = false;
        this._bShowFilteredTasks = false;
        this._newTask = '';
        this._sortType = '';
        this._filterType = '';
        var baseTasks = [
            { id: 1, title: 'Create a Todo app for SOO Media', finished: true },
            { id: 2, title: 'Show the Todo app to Bart', finished: false },
            { id: 3, title: 'Acquire a new job! :)', finished: false }
        ];
        for (var i = 0, j = baseTasks.length; i < j; ++i) {
            var newTask = new task_component_1.Task();
            newTask.id = baseTasks[i].id;
            newTask.title = baseTasks[i].title;
            newTask.finished = baseTasks[i].finished;
            this._tasks.push(newTask);
            ++this._tasksLength;
        }
    }
    TasksComponent.prototype.addTask = function () {
        if (this._newTask.length < 5) {
            alert('Title needs to be at least 5 characters long');
            return;
        }
        ++this._tasksLength;
        var task = new task_component_1.Task();
        task.id = this._tasksLength;
        task.title = this._newTask;
        this._tasks.push(task);
        this._newTask = '';
        this.resortOrRefilter();
    };
    TasksComponent.prototype.sortByFinished = function (task1, task2) {
        if (task1.finished < task2.finished)
            return 1;
        if (task1.finished > task2.finished)
            return -1;
        return 0;
    };
    TasksComponent.prototype.sortByUnfinished = function (task1, task2) {
        if (task1.finished < task2.finished)
            return -1;
        if (task1.finished > task2.finished)
            return 1;
        return 0;
    };
    TasksComponent.prototype.sortById = function (task1, task2) {
        if (task1.id < task2.id)
            return -1;
        if (task1.id > task2.id)
            return 1;
        return 0;
    };
    TasksComponent.prototype.filterByFinished = function (task) {
        return task.finished === true;
    };
    TasksComponent.prototype.filterByUnfinished = function (task) {
        return task.finished === false;
    };
    TasksComponent.prototype.resetFilter = function (task) {
        return (task.finished === true || task.finished === false);
    };
    TasksComponent.prototype.sortTasks = function (sortType) {
        this._sortType = sortType;
        switch (sortType) {
            case "none":
                this._tasks.sort(this.sortById);
                break;
            case "finished":
                this._tasks.sort(this.sortByFinished);
                break;
            case "unfinished":
                this._tasks.sort(this.sortByUnfinished);
                break;
            default:
                this._tasks.sort(this.sortById);
        }
    };
    TasksComponent.prototype.filterTasks = function (filterType) {
        this._filteredTasks = this._tasks;
        this._filterType = filterType;
        this._bShowFilteredTasks = true;
        switch (filterType) {
            case "none":
                this._filteredTasks = this._filteredTasks.filter(this.resetFilter);
                break;
            case "finished":
                this._filteredTasks = this._filteredTasks.filter(this.filterByFinished);
                break;
            case "unfinished":
                this._filteredTasks = this._filteredTasks.filter(this.filterByUnfinished);
                break;
            default:
                this._filteredTasks = this._filteredTasks.filter(this.resetFilter);
        }
    };
    TasksComponent.prototype.resortOrRefilter = function () {
        if (this._bShowFilteredTasks) {
            this.filterTasks(this._filterType);
        }
        else if (!this._bShowFilteredTasks) {
            this.sortTasks(this._sortType);
        }
    };
    TasksComponent.prototype.toggleAddForm = function () {
        this._bShowSortForm = false;
        this._bShowFilterForm = false;
        this._bShowFilteredTasks = false;
        this._bShowAddForm = true;
    };
    TasksComponent.prototype.toggleSortForm = function () {
        this._bShowAddForm = false;
        this._bShowFilterForm = false;
        this._bShowFilteredTasks = false;
        this._bShowSortForm = true;
    };
    TasksComponent.prototype.toggleFilterForm = function () {
        this._bShowAddForm = false;
        this._bShowSortForm = false;
        this._bShowFilterForm = true;
    };
    TasksComponent.prototype.onDelete = function (id) {
        var confirmDelete = confirm('Are you sure you want to delete this task?');
        if (confirmDelete) {
            this._tasks = this._tasks.filter(function (task) {
                return task.id !== id;
            });
        }
        this.resortOrRefilter();
    };
    TasksComponent.prototype.onFinished = function (id) {
        this._tasks.find(function (task) {
            if (task.id === id) {
                task.finished = !task.finished;
            }
            return task.id === id;
        });
        this.resortOrRefilter();
    };
    TasksComponent = __decorate([
        core_1.Component({
            selector: 'tasks-list',
            moduleId: module.id,
            templateUrl: 'tasks.template.html',
            styleUrls: ['tasks.style.css'],
            directives: [task_component_1.Task]
        }), 
        __metadata('design:paramtypes', [])
    ], TasksComponent);
    return TasksComponent;
}());
exports.TasksComponent = TasksComponent;
