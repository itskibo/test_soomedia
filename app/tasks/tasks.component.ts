import {Component} from '@angular/core';
import {Task} from './../task/task.component';

@Component({
	selector: 'tasks-list',
	moduleId: module.id,
	templateUrl: 'tasks.template.html',
	styleUrls: ['tasks.style.css'],
	directives: [Task]
})
export class TasksComponent {
	_tasks: Array<Task> = [];
	_filteredTasks: Array<Task> = [];
	_tasksLength: number = 0;

	_bShowAddForm: boolean = true;
	_bShowSortForm: boolean = false;
	_bShowFilterForm: boolean = false;
	_bShowFilteredTasks: boolean = false;

	_newTask: string = '';

	_sortType: string = '';
	_filterType:string = '';

	constructor() {
		let baseTasks = [
			{id: 1, title: 'Create a Todo app for SOO Media', finished: true},
			{id: 2, title: 'Show the Todo app to Bart', finished: false},
			{id: 3, title: 'Acquire a new job! :)', finished: false}
		];

		for(let i = 0, j = baseTasks.length; i < j; ++i) {
			let newTask = new Task();

			newTask.id = baseTasks[i].id;
			newTask.title = baseTasks[i].title;
			newTask.finished = baseTasks[i].finished;

			this._tasks.push(newTask);

			++this._tasksLength;
		}
	}

	addTask() {
		if (this._newTask.length < 5) {
			alert('Title needs to be at least 5 characters long');
			return;
		}

		++this._tasksLength;

		let task = new Task();

		task.id = this._tasksLength;
		task.title = this._newTask;

		this._tasks.push(task);

		this._newTask = '';

		this.resortOrRefilter();
	}

	sortByFinished(task1 : Task, task2 : Task) {
		if (task1.finished < task2.finished) return 1;
		if (task1.finished > task2.finished) return -1;
		return 0;
	}

	sortByUnfinished(task1 : Task, task2 : Task) {
		if (task1.finished < task2.finished) return -1;
		if (task1.finished > task2.finished) return 1;
		return 0;
	}

	sortById(task1 : Task, task2 : Task) {
		if (task1.id < task2.id) return -1;
		if (task1.id > task2.id) return 1;
		return 0;
	}

	filterByFinished(task) {
		return task.finished === true;
	}

	filterByUnfinished(task) {
		return task.finished === false;
	}

	resetFilter(task) {
		return (task.finished === true || task.finished === false);
	}

	sortTasks(sortType : string) {
		this._sortType = sortType;

		switch(sortType) {
			case "none":
				this._tasks.sort(this.sortById);
				break;

			case "finished":
				this._tasks.sort(this.sortByFinished);
				break;

			case "unfinished":
				this._tasks.sort(this.sortByUnfinished);
				break;
			default:
				this._tasks.sort(this.sortById);
		}
	}

	filterTasks(filterType : string) {
		this._filteredTasks = this._tasks;
		this._filterType = filterType;

		this._bShowFilteredTasks = true;

		switch(filterType) {
			case "none":
				this._filteredTasks = this._filteredTasks.filter(this.resetFilter);
				break;
			case "finished":
				this._filteredTasks = this._filteredTasks.filter(this.filterByFinished);
				break;
			case "unfinished":
				this._filteredTasks = this._filteredTasks.filter(this.filterByUnfinished);
				break;
			default:
				this._filteredTasks = this._filteredTasks.filter(this.resetFilter);
		}
	}

	resortOrRefilter() {
		if (this._bShowFilteredTasks) {
			this.filterTasks(this._filterType);
		} else if (!this._bShowFilteredTasks) {
			this.sortTasks(this._sortType);
		}
	}

	toggleAddForm() {
		this._bShowSortForm = false;
		this._bShowFilterForm = false;
		this._bShowFilteredTasks = false;

		this._bShowAddForm = true;
	}

	toggleSortForm() {
		this._bShowAddForm = false;
		this._bShowFilterForm = false;
		this._bShowFilteredTasks = false;

		this._bShowSortForm = true;
	}

	toggleFilterForm() {
		this._bShowAddForm = false;
		this._bShowSortForm = false;
		this._bShowFilterForm = true;
	}

	onDelete(id : number) {
		let confirmDelete = confirm('Are you sure you want to delete this task?');
		if (confirmDelete) {
			this._tasks = this._tasks.filter(function(task) {
				return task.id !== id;
			});
		}

		this.resortOrRefilter();
	}

	onFinished(id : number) {
		this._tasks.find(function(task) {
			if (task.id === id) {
				task.finished = !task.finished;
			}

			return task.id === id;
		});

		this.resortOrRefilter();
	}
}