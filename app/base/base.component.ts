import {Component} from '@angular/core';
import {TasksComponent} from './../tasks/tasks.component';

@Component({
	selector: 'app',
	moduleId: module.id,
	templateUrl: 'base.template.html',
	styleUrls: ['base.style.css'],
	directives: [TasksComponent]
})
export class BaseComponent{
	title = 'SOO Media Todo';
}